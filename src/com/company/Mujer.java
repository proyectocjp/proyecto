package com.company;

/**
 * Created by Miguel on 08/06/17.
 */
public class Mujer extends Persona {        //herencia de clases
    public char sexo = 'F';

    class InnerMujer{    //Implementación de una Inner Class
        public int imprimiredad(){
            System.out.println(edad);
            return edad;
        }
    }

    public Mujer(String nombre, int edad,double pesoKilogramos, int alturaCentimetros,char sexo){
        this.nombre=nombre;
        this.edad=edad;
        this.sexo=sexo;
        this.pesoKilogramos=pesoKilogramos;
        this.alturaCentimetros=alturaCentimetros;
        this.alturaMetros = alturaCentimetros/100.00;


    }




    public double indiceMasaMujer(double kg, double m){
        kg = pesoKilogramos;
        m = alturaMetros;
        double imc = kg/(m*m);
        return imc;                                                                                                 /*


        //Instancia de la Inner Class
        Mujer mujer = new Mujer("Laura",3,45,183,'F');
        Mujer.InnerMujer innerMujer = mujer.new InnerMujer();
        System.out.println(innerMujer.imprimiredad());                                  */
    }


    @Override
    public double indM() {
        double kg = pesoKilogramos;
        double m = alturaMetros;
        double imc = kg/(m*m);
        return imc;
    }
}
