package com.company;
import java.util.*;


public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int options = 0;

        List<Hombre> hombreList = new ArrayList<Hombre>();
        List<Mujer> mujerList = new ArrayList<Mujer>();

        Map mapIMC = new HashMap();

        do {
            System.out.println("MENU");
            System.out.println("1. Ingrese Persona");
            System.out.println("2. Imprimir Personas");
            System.out.println("3. Calcula Indice de Masa");
            System.out.println("4. Consultar Resultado IMC Persona");
            System.out.println("5. Salir");
            options = sc.nextInt();

            //assert options >= 0: options = 0;
            System.out.println("Escogio Opcion: " + options);
            String nombre;
            String edad;
            String peso;
            String altura;
            String sexo;

            switch (options) {
                case 1:
                    System.out.println("Ingrese Datos");
                    System.out.print("Nombre: ");
                    sc.nextLine();
                    nombre = sc.nextLine();
                    System.out.print("\nEdad: ");
                    edad = sc.nextLine();
                    System.out.print("\npeso (Kg): ");
                    peso = sc.nextLine();
                    System.out.print("\nAltura (cm): ");
                    altura = sc.nextLine();
                    System.out.print("\nSexo (masculino o femenino): ");
                    sexo = sc.nextLine();

                    sexo = sexo.toUpperCase();
                    char sx = sexo.charAt(0);
                    //System.out.print(sx);
                    boolean check = true;

                    if(sx == 'M')
                        check = true;
                    else {
                        if(sx == 'F')
                            check = true;
                        else
                            check = false;
                    }



                    if(check == false)
                    {
                        System.out.println("\nSolo puede ingresar masculino o femenino en sexo, vuelva ingresar persona\n");
                        break;
                    }
                    Integer iedad = new Integer(0);
                    try{
                        iedad = new Integer(edad);
                    }catch (Exception e){
                        check = false;
                    }

                    if(check==false)
                    {
                        System.out.println("\nSolo puede ingresar valores enteros para la edad, Vuelva ingresar Persona\n");
                        break;
                    }

                    Double dpeso = new Double(0);
                    try{
                        dpeso = new Double(peso);
                    }catch (Exception e){
                        check = false;
                    }

                    if(check==false)
                    {
                        System.out.println("\nSolo puede ingresar valores dobles para el peso, Vuelva ingresar Persona\n");
                        break;
                    }

                    Integer ialtura = new Integer(0);

                    try{
                        ialtura = new Integer(altura);
                    }catch (Exception e){
                        check = false;
                    }

                    if(check==false)
                    {
                        System.out.println("\nSolo puede ingresar valores enteros para la altura, Vuelva ingresar Persona\n");
                        break;
                    }

                    if(sx == 'M') {
                        Hombre h1 = new Hombre(nombre, iedad.intValue(), dpeso.doubleValue(), ialtura.intValue(), sx);
                        hombreList.add(h1);
                        System.out.println("\nPersona HOMBRE Ingresada\n");
                        break;
                    }

                    if(sx == 'F'){
                        Mujer m2 = new Mujer(nombre, iedad.intValue(), dpeso.doubleValue(), ialtura.intValue(), sx);
                        mujerList.add(m2);
                        System.out.println("\nPersona MUJER Ingresada\n");
                        break;
                    }

                    //System.out.println(nombre+edad+peso+altura+sx);
                case 2:
                    int options2;
                    do{
                        System.out.println("Seleccione la opcion");
                        System.out.println("1. Imprimir HOMBRES");
                        System.out.println("2. Imprimir MUJERES");
                        System.out.println("3. VOLVER");
                        options2 = sc.nextInt();
                        switch (options2){
                            case 1:
                                int hz = hombreList.size();
                                for(int i=0; i<hz; i++){
                                    System.out.println("Nombre: " + hombreList.get(i).nombre + " Edad: " + hombreList.get(i).edad
                                            + " peso: " + hombreList.get(i).pesoKilogramos
                                            + " Altura(m): " + hombreList.get(i).alturaMetros);
                                }
                                break;
                            case 2:
                                int mz = mujerList.size();
                                for(int i=0; i<mz; i++){
                                    System.out.println("Nombre: " + mujerList.get(i).nombre + " Edad: "
                                            + mujerList.get(i).edad + " peso: " + mujerList.get(i).pesoKilogramos
                                            + " Altura(m): " + mujerList.get(i).alturaMetros);
                                }
                                break;
                            case 3:
                                System.out.println("\nVOLVER MENU\n");
                                break;
                            default:
                                System.out.println("\nOPCION INVALIDA\n");
                        }
                    }while (options2!=3);
                    break;
                case 3:
                    System.out.println("Ingrese nombre de la persona que quiere calcular el indice de masa");
                    sc.nextLine();
                    String busqnombre = sc.nextLine();
                    boolean existeh = false;
                    boolean existem = false;

                    double imc;

                    int hz = hombreList.size();
                    int mz = mujerList.size();
                    int indexh = -1;
                    int indexm = -1;

                    for(int i = 0; i<hz; i++){
                        if(busqnombre.equalsIgnoreCase(hombreList.get(i).nombre)){
                            existeh = true;
                            indexh = i;
                        }
                    }

                    for(int i = 0; i<mz; i++){
                        if(busqnombre.equalsIgnoreCase(mujerList.get(i).nombre)){
                            existem = true;
                            indexm = i;
                        }
                    }

                    if(existeh == false && existem == false){
                        System.out.println("No existe la persona ingresada");
                        break;
                    }
                    else{
                        if (existeh == true){
                            Hombre h1 = hombreList.get(indexh);
                            imc =  h1.indM();
                            String resultadoIMCh = resultadoIMCHombre(imc);
                            mapIMC.put(h1.nombre, resultadoIMCh);
                            System.out.println("Nombre: " + h1.nombre + " IMC: " + imc);
                            break;
                        }
                        if(existem == true){
                            Mujer m1 = mujerList.get(indexm);
                            imc = m1.indM();
                            String resultadoIMCm = resultadoIMCMujer(imc);
                            mapIMC.put(m1.nombre, resultadoIMCm);
                            System.out.println("Nombre: " + m1.nombre + " IMC: " + imc);
                            break;
                        }
                    }

                case 4:
                    mostrarTodos(mapIMC.keySet());
                    mostrarTodos(mapIMC.values());
                    break;

                case 5:
                    System.out.println("GRACIAS POR USAR EL PROGRAMA");
                    break;
                default:
                    System.out.println("Opcion Invalida");
                    break;
            }
        }while (options != 5);


       /* Mujer m1 = new Mujer("Alejandra",21, 50.6, 165, 'F');
        Hombre h = new Hombre("Juan",24, 70.2, 170, 'M');
        //Indice de masa por metodo

        if (h.alturaCentimetros<20){
            h.alturaCentimetros=h.alturaCentimetros*100;

        }

        double imc = h.indiceMasaHomre(h.pesoKilogramos, h.alturaMetros);

        //Indice de Masa por interface
        double imc1 = h.indM();

        LimiteEMCHombre lmh;

        String resultHombre = resultadoIMCHombre(imc1);
        String reslutMujer = resultadoIMCMujer(m1.indM());

        System.out.println("Nombre: "+h.nombre+" \nEdad: "+h.edad+" \nAltura: "+h.alturaMetros+" \nPeso: "+h.pesoKilogramos+"\nÍndice de Masa Corporal: "+imc1+"\nResultado: "+ resultHombre +"\n");
        System.out.println("Nombre: "+m1.nombre+" \nEdad: "+m1.edad+" \nAltura: "+m1.alturaMetros+" \nPeso: "+m1.pesoKilogramos+"\nÍndice de Masa Corporal: "+m1.indM()+"\nResultado: " + reslutMujer);
        */
    }

    public static String resultadoIMCHombre(double imc){
        LimiteEMCHombre lh = null;
        String res;
        if(imc <= 20.0){
            lh = lh.BAJOPESO;
        }
        if(imc >20.0 && imc<=24.9){
            lh = lh.NORMAL;
        }
        if(imc >24.9 && imc<=29.9){
            lh = lh.OBESIDADLEVE;
        }
        if(imc >29.9 && imc<=40.0){
            lh = lh.OBESIDADSEVERA;
        }
        if(imc >40.0 && imc<=100.0){
            lh = lh.OBESIDADMUYSEVERA;
        }
        res = lh.name().toString();
        return res;
    }

    public static String resultadoIMCMujer(double imc){
        LimiteEMCMujer lm = null;
        String res;
        if(imc <= 20.0){
            lm = lm.BAJOPESO;
        }
        if(imc >20.0 && imc<=23.9){
            lm = lm.NORMAL;
        }
        if(imc >23.9 && imc<=28.9){
            lm = lm.OBESIDADLEVE;
        }
        if(imc >28.9 && imc<=37.0){
            lm = lm.OBESIDADSEVERA;
        }
        if(imc >37.0 && imc<=100.0){
            lm = lm.OBESIDADMUYSEVERA;
        }
        res = lm.name().toString();
        return res;
    }
    static void mostrarTodos(Collection c){
        Iterator it=c.iterator();
        while(it.hasNext()){
            System.out.print("   "+it.next());

        }
        System.out.println("");
    }


}
