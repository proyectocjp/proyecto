package com.company;

/**
 * Created by david on 13/6/2017.
 */
public enum LimiteEMCMujer {
    BAJOPESO(20.0),
    NORMAL(23.9),
    OBESIDADLEVE(28.9),
    OBESIDADSEVERA(37),
    OBESIDADMUYSEVERA(100);


    public final double limiteMujer;

    LimiteEMCMujer(double limiteMujer) {
        this.limiteMujer = limiteMujer;
    }

    public double getLimiteMujer() {
        return limiteMujer;
    }
}
