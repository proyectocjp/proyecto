package com.company;

/**
 * Created by Miguel on 08/06/17.
 */
public abstract class Hombre extends Persona { //herencia de la clase Persona
    public char sexo = 'M';

     class InnerHombre{    //Implementación de una Inner Class
        public int imprimiredad(){
            System.out.println(edad);
            return edad;
        }
    }

    void desplegarInner(){
        InnerHombre innerHombre = new InnerHombre();
        innerHombre.imprimiredad();
    }

    public Hombre(String nombre, int edad,double pesoKilogramos, int alturaCentimetros,char sexo){
        this.nombre=nombre;
        this.edad=edad;
        this.sexo=sexo;
        this.pesoKilogramos=pesoKilogramos;
        this.alturaCentimetros=alturaCentimetros;
        this.alturaMetros = alturaCentimetros/100.00;          //casteo implícito
    }

    public double indiceMasaHomre(double kg, double m){
        kg = pesoKilogramos;
        m = alturaMetros;
        double imc = kg/(m*m);             // uso de operadores
        return imc;                                     /*

        //Instancia de la clase Inner
        Hombre hombre=new Hombre();
        Hombre.InnerHombre innerHombre = hombre.new InnerHombre();
        System.out.println(innerHombre.imprimiredad());             */
    }

    @Override
    public double indM() {            //Sobreescritura
        double kg = pesoKilogramos;
        double m = alturaMetros;
        double imc = kg/(m*m);
        return imc;


    }
}
