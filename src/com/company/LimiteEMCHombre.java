package com.company;

/**
 * Created by david on 13/6/2017.
 */
public enum LimiteEMCHombre {
    BAJOPESO(20.0),
    NORMAL(24.9),
    OBESIDADLEVE(29.9),
    OBESIDADSEVERA(40.0),
    OBESIDADMUYSEVERA(100.0);


    public final double limiteHombre;

    LimiteEMCHombre(double limiteHombre) {
        this.limiteHombre = limiteHombre;
    }

    public double getLimiteHombre() {
        return limiteHombre;
    }
}
